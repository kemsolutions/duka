<?php

/**
 * The end of the road!
 */

Route::get('/{any}', function ($any) {

  return redirect('https://lapara.ca/');

})->where('any', '.*');
